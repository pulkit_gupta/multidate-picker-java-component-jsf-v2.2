/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist;

import Models.AirportBooking;
import Models.BookingLockingDays;
import Models.ParkArea;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author pulkit
 */
public class DAOLockedBookings {
    
    //used to access hibernate API DB
    private static SessionFactory factory;
    
    public DAOLockedBookings(){
         factory = HibernateUtil.getSessionFactory();
    }                
        
    public List<BookingLockingDays> getProviderBookings(int parkAreaID, Date today, Date till) {

        List<BookingLockingDays> bookings = null;

        System.out.print("Getting all the bookings from provider");
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM BookingLockingDays WHERE parkareaid = :parkAreaId AND status != 'OFF' AND till_date > :currentDate AND from_date < :tillRangeDate");

            /*
             System.out.println("Current Date is: " + dateFormat.format(TODAY));
             System.out.println("After 90 Days is: " + dateFormat.format(TILL));
             */                        
            
            query.setInteger("parkAreaId", parkAreaID);
            query.setDate("currentDate", today);
            query.setDate("tillRangeDate", till);
            
            bookings = query.list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return bookings;
    }

    
    public int getQuotaByParkAreaID(int parkAreaID){
        
        ParkArea area = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM ParkArea WHERE parkareaid = :parkId");

            query.setInteger("parkId", parkAreaID);
           
            area = (ParkArea) query.uniqueResult();
            
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        int value =0;
        
        if(area!=null){
            value = area.getNrOfSpaces();
        }
        
        return value;
    }
    
    public List<AirportBooking> getAirportBookings(int parkAreaID, Date today, Date till) {

        List<AirportBooking> bookings = null;

        System.out.print("Getting all the bookings from airportbooking");
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM AirportBooking WHERE parkareaid = :parkAreaId AND arrival > :currentDate AND departure < :tillRangeDate");
            
            query.setInteger("parkAreaId", parkAreaID);            
            query.setDate("currentDate", today);
            query.setDate("tillRangeDate", till);
            bookings = query.list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return bookings;
    }     
    
}
