package de.parkinglist;

/**
 * Main Controller file for managing data on views 
 *
 */
import java.util.Calendar;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author pulkitgupta
 */
@ManagedBean(name = "dataController")
@RequestScoped
public class DataController {
    
    final int range = 90;
    
    //hidden field for Locked date shower. Contains comma seperated dated for the locked
    private String dates;
 
    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    
    public DataController() {
        
        this.dates = "";
        
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.DATE, range);
        Date till = cal.getTime();             
               
        LockedDates lockedDates = new LockedDates(today, till);        
        
        
        this.dates = lockedDates.getDatesBYParkAreaID(3101);         
        
//        System.out.println("price is "+new DAOPriceList().getpriceFromList(1));
        
    }
}
