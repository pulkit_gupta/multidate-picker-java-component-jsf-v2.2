/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist;

import java.util.Calendar;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author pulkit
 */

@ManagedBean(name = "calendarView")
@RequestScoped
public class CalenderView {
    
    private Date fromDate;
    private Date toDate;
    private Date toDateMin;
    private Date fromDateMin;   
    
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDateMin() {
        return toDateMin;
    }

    public void setToDateMin(Date toDateMin) {
        this.toDateMin = toDateMin;
    }

    public Date getFromDateMin() {
        return fromDateMin;
    }

    public void setFromDateMin(Date fromDateMin) {
        this.fromDateMin = fromDateMin;
    }
    
    public CalenderView(){
        
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        
        cal.add(Calendar.DATE, 7);
        Date till = cal.getTime();
        
        this.fromDate = today;
        
        this.toDate = till;
        
        this.fromDateMin = today;
        this.toDateMin = today;
        
    }
    public void onDateSelect(SelectEvent e){
                        
        this.toDateMin = this.fromDate;  
        
        if(toDateMin.compareTo(toDate) > 0){
            toDate = toDateMin;
        }
        
    }
    
    public void submit(){
        
        
        new PriceCalculator().getPrice(fromDate, toDate, 1);
        
        this.price = 100;
    }
}
