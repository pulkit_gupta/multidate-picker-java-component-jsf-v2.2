/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist;

import Models.AirportBooking;
import Models.AirportPricing;
import Models.BookingLockingDays;
import Models.ParkArea;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author pulkit
 */
public class DAOPriceList {

    //used to access hibernate API DB
    private static SessionFactory factory;

    public DAOPriceList() {
        factory = HibernateUtil.getSessionFactory();
    }

    
    public List<AirportPricing> getpriceFromList(int parkAreaId) {

        List<AirportPricing> airportPrice = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            
            Query query = session.createQuery(" FROM AirportPricing WHERE parkareaid = :parkAreaId");
            
            query.setInteger("parkAreaId", parkAreaId);
            
            airportPrice = query.list();

            tx.commit();
        
        } catch (HibernateException e) {
            
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            
        } finally {
            session.close();
        }
     
        return airportPrice;
    }

}
