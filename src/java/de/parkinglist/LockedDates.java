/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist;

import Models.AirportBooking;
import Models.BookingLockingDays;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author pulkit
 */
public class LockedDates {

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final Date TODAY, TILL;

    private final DAOLockedBookings dao_lockingDB;

    public HashMap<Date, Integer> datemp;    

    private String dates;

    public LockedDates(Date today, Date tillDate) {
        this.datemp = new HashMap<>();
        this.dates = "";
        this.TODAY = today;
        this.TILL = tillDate;

        //get DB object
        this.dao_lockingDB = new DAOLockedBookings();
        
        
    }

    public String getDatesBYParkAreaID(int parkAreaID) {

        String datesByUser = getDatesFromUser(parkAreaID);

        String datesByProvider = getDatesFromProvider(parkAreaID);
        
        System.out.println("Dates by USER to block ist "+datesByUser);
        
        System.out.println("Dates by provider to block ist "+datesByProvider);
        
        System.out.println("total by provider to block ist "+datesByUser+","+datesByProvider);
        //merge with the provider
        return datesByUser+","+datesByProvider;
        
    }

    private String getDatesFromProvider(int parkAreaID) {
       
        //accesssing database
        List<BookingLockingDays> bookings = dao_lockingDB.getProviderBookings(parkAreaID, TODAY, TILL);
        
        for (BookingLockingDays booking : bookings) {
            
            populateLockingDates(booking.getFrom_date(), booking.getTill_date(), false);
            /*
             System.out.print("ID: " + booking.getLockingdaysid());
             System.out.print("  From: " + booking.getFrom_date());
             System.out.print("  To: " + booking.getTill_date());
             */
        }
        
        if (!dates.isEmpty()) {
            dates = dates.substring(1);
        }

        //System.out.println("VALUE IS"+ dates);
        
        return dates;
    }

    private String getDatesFromUser(int parkAreaID) {

        int quota = 2;

        //accesssing database
        List<AirportBooking> bookings = dao_lockingDB.getAirportBookings(parkAreaID, TODAY, TILL);

        quota = dao_lockingDB.getQuotaByParkAreaID(parkAreaID);
        System.out.println("value of capacity of "+String.valueOf(parkAreaID)+" is"+ String.valueOf(quota));
        
        String datesString = "";
        if(bookings!=null){
            for (AirportBooking booking : bookings) {

                populateLockingDates(booking.getDeparture(), booking.getArrival(), true);
                /*
                 System.out.print("ID: " + booking.getLockingdaysid());
                 System.out.print("  From: " + booking.getFrom_date());
                 System.out.print("  To: " + booking.getTill_date());
                 */
            }

            for (Date key : datemp.keySet()) {
                int value = datemp.get(key);
                //System.out.println(key + " " + value);

                if (value >= quota) {
                    datesString += "," + dateFormat.format(key);
                }
            }

            if (!datesString.isEmpty()) {
                datesString = datesString.substring(1);
            }

        }
        
        return datesString;
    }

    //used for array and hashmap populating for the range of the from and to dates of any table
    private void populateLockingDates(Date fromDate, Date toDate, boolean isHash) {
            
        if (toDate.after(TILL)) {
            toDate = TILL;
        }
        if (fromDate.before(TODAY)) {
            fromDate = TODAY;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(toDate);
        cal2.add(Calendar.DATE, 1);

        while (cal.getTime().before(cal2.getTime())) {
            
            if (isHash) {
                if (datemp.containsKey(cal.getTime())) {
                    Integer oldInteger = datemp.get(cal.getTime());
                    datemp.put(cal.getTime(), oldInteger + 1);
                } else {

                    datemp.put(cal.getTime(), 1);
                }
            } else {
                this.dates += "," + dateFormat.format(cal.getTime());                                
            }

            cal.add(Calendar.DATE, 1);
            //leave the last date its the plus one.
        }
        
    }

}
