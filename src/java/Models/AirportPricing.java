/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "airportpricing")

public class AirportPricing {
    
    @Id
    @GeneratedValue
    @Column(name = "pricingid")
    private int pricingId;
    
    @Column(name = "parkareaid")
    private int parkareaId;
    
    
    @Column(name = "month")
    private int month;
    
    
    @Column(name="status")
    private String status;
    
    
    @OneToMany     
    @JoinColumn(name="pricelistid")
    @OrderBy("nbrday")
    private List <PriceList> priceList;

    public List<PriceList> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<PriceList> priceList) {
        this.priceList = priceList;
    }
           
    

    public int getPricingId() {
        return pricingId;
    }

    public void setPricingId(int pricingId) {
        this.pricingId = pricingId;
    }

    public int getParkareaId() {
        return parkareaId;
    }

    public void setParkareaId(int parkareaId) {
        this.parkareaId = parkareaId;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
     
    
    
    
}

