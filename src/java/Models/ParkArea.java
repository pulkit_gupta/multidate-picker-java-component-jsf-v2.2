/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author pulkit
 */


@Entity 
@Table(name = "parkarea")
public class ParkArea implements Serializable{

    @Id
    @GeneratedValue
    @Column(name = "parkareaid")
    private int parkAreaId;

    
    @Column(name = "nrofspaces")
    private int nrOfSpaces;
    
    public int getParkAreaId() {
        return parkAreaId;
    }

    public void setParkAreaId(int parkAreaId) {
        this.parkAreaId = parkAreaId;
    }

    public int getNrOfSpaces() {
        return nrOfSpaces;
    }

    public void setNrOfSpaces(int nrOfSpaces) {
        this.nrOfSpaces = nrOfSpaces;
    }

           
}