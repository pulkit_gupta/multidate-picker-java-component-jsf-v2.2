/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author pulkitgupta
 */

@Entity 
@Table(name = "booking_locking_days")
public class BookingLockingDays implements Serializable {
    
    @Id
    @GeneratedValue
    @Column(name = "lockingdaysid")
    private int lockingdaysid;  

    @Column(name = "from_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date from_date;
    
    @Column(name = "till_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date till_date;

    public int getLockingdaysid() {
        return lockingdaysid;
    }

    public void setLockingdaysid(int lockingdaysid) {
        this.lockingdaysid = lockingdaysid;
    }

    
    public Date getFrom_date() {
        return from_date;
    }

    public void setFrom_date(Date from_date) {
        this.from_date = from_date;
    }

    public Date getTill_date() {
        return till_date;
    }

    public void setTill_date(Date till_date) {
        this.till_date = till_date;
    }  
    
}
