/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author pulkit
 */
@Entity 
@Table(name = "airportbooking")
public class AirportBooking implements Serializable{

    @Id
    @GeneratedValue
    @Column(name = "bookingid")
    private int bookingId;

    @Column(name = "parkareaid")
    private int parkAreaId;
               
    @Column(name = "departure")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date departure;

    @Column(name = "arrival")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date arrival;

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public int getParkAreaId() {
        return parkAreaId;
    }

    public void setParkAreaId(int parkAreaId) {
        this.parkAreaId = parkAreaId;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }
    
    
}
