# README #

This document contains steps are necessary to get your application up and running.

### Project Multi date picker ###

* This is a project which will enable user to choose multiple dates on GUI and save them to DB
* DB is mysql for now
* There are also some pages which uses prime faces components for displaying the Data Beans

### Set Up ###

* Install Netbeans IDE
* Import Project from this git repository. Either download or use Git Clone
* Install Glashfish if netbeans does not have bundles
* Install Mysql or get MAMP/Wamp depending upon OS
* Start Mysql and make a DB called parkinglist
* Compile and run the code in any browser. (PS: It may be possible that a change in hibernate config file is needed.)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact /Suggestion###

* Pulkit Gupta
* pulkit.itp@gmail.com